<?php
namespace Leomax\Logger\Loggings;

use Monolog\Logger;
use Monolog\Handler\TelegramBotHandler;

/*
 * Класс фабрика для расширения функционала. Channel получает отсюда фействие.
 * Handler способен задавать свой стиль сообщений.
 */
class TelegramLogger
{
    public function __invoke(array $config)
    {
        $logger = new Logger('telegram');
        $logger->pushHandler(new TelegramBotHandler(
            getenv('TELEGRAM_BOT_TOKEN'),
            getenv('TELEGRAM_CHAT_ID'),
            Logger::INFO
        ));
        return $logger;
    }
}
