<?php

use Leomax\Logger\Loggings\TelegramLogger;

return [
    'telegram' => [
        'driver' => 'custom',
        'via' => TelegramLogger::class,
    ],
    'sentry' => [
        'driver' => 'sentry',
    ],
];
