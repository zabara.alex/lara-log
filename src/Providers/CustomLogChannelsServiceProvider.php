<?php
namespace Leomax\Logger\Providers;

use Illuminate\Support\ServiceProvider;
use Monolog\Handler\TelegramBotHandler;
use Monolog\Logger;

class CustomLogChannelsServiceProvider extends ServiceProvider
{
    public function boot()
    {
        // Добавляем каналы.
        $this->mergeConfigFrom(__DIR__. '/custom-log-channels.php', 'logging.channels');

        // Добавляем на канал дополнительные зависимости.
        $this->mergeConfigFrom(__DIR__. '/add-channels.php', 'logging.channels.stack.channels');
    }
    public function register()
    {
        //
    }
}
